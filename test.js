var server = require('./server.js');
var assert = require('assert');


describe('Section Functions', function(){
	var name = 'name1'; 
	var data = [];
	var id;
	describe('createSection', function(){
		it('should save the document', function (){
			id = server.createSection(name, data);
			assert (id != null);
			assert(data.length>0);
			assert(data[0].name='name1');
		});
	});
	describe('updateSection', function(){
		var new_section = JSON.stringify({id: id, name: 'name2'});
		it('should save the document', function (){
			server.updateSection(id, new_section, data);
			assert(data[0].name='name2');
			assert(data.length > 0);
		});
	});
	describe('deleteSection', function(){
		it('should save the document', function (){
			server.deleteSection(id, data);
			assert(data.length === 0);
		});
	});
});


