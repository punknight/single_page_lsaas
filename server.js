var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var serveStatic = require('serve-static');
var mongoose = require("mongoose");

mongoose.Promise = global.Promise;
var env = process.env.NODE_ENV || 'development';
if(env==='development'){
	mongoose.connect("mongodb://localhost/lsaas", { useMongoClient: true })
	.then(function(){
    console.log('connected to local database!');
  })
  .catch(function(err){
  	console.log('not connected to db, maybe mongo is not on');
  })    	
} else {
	mongoose.connect('mongodb://punknight:Fish16@ds111940.mlab.com:11940/lsaas_prod', { useMongoClient: true })
	.then(function(){
    console.log('connected to external database!');
  })
  .catch(function(err){
  	console.log('not connected to db, maybe mongo is not on');
  })    
}

  

var data = [];
var db_id;

//DB STUFF
var Schema = mongoose.Schema;
var dataSchema = new Schema({
	data: [],
	is_active: Boolean
});

dataSchema.statics.persistData = function(db_id, data_arr){
	Data.findById(db_id, function(err, data_obj) {
  	if (err) throw err;
  	data_obj.data = data_arr;
  	data_obj.save()
  		.catch(function(err){ console.log('err updating')})
  		.then(function(){console.log('successfully updated')})
  });
};

dataSchema.statics.bootstrapDB = function(){
	Data.findOne({}, function(err, data_obj) {
	  if (err) throw err;
	  if (data_obj===null){
	  	var new_data_obj = new Data({
	  		data:[
						{id: 's1', name: 'user', disabled: true, data: [
							{id: 'u1', name: 'user1', space_after: 'space', connections: [], data: [
								{id: "btn0", type: 'user', state: 0, table: [
									{class:'', row: [{render: 'Lauren'}]},
									{class:'', row: [{render: 'lmiller@lawfirm.com'}]}
								]},
							]},
						]},
						{id: 's2', name: 'task', disabled: true, data: [
							{id: 't1', name: 'task1', space_after: 'space', connections: ['p1'], data: [
								{id: 'btn3', type: 'task', state: 0, table: [
									{class:'', row:[{render: 'March 3, 2017', type:'date', date: [2017, 2, 3]}]},
									{class:'', row:[{render: 'Receive case'}]}
								]}
							]}
						]},
						{id: 's3', name: 'policy', disabled: true, data: [
							{id: 'p1', name: 'policy1', space_after: 'space', connections: ['t1'], data: [
								{id: 'btn6', type: 'policy', state: 0, table: [
									{class: '', name_key: 'task1', row: [{render: '0 days', type: 'time', add_time: [0, 0, 0], to_task: 'task1', state: 0}, {render: "Receive Case"}]},
									{class: '', name_key: 'task2', row: [{render: '6 days', type: 'time', add_time: [0, 0, 6], to_task: 'task1', state: 1}, {render: "Initial Status Letter"}]},
									{class: '', name_key: 'task3', row: [{render: '12 days', type: 'time', add_time: [1, 0, 12], to_task: 'task1', state: 2}, {render: "Preservation Letter"}]},
								]}
							]},
						]}
					], is_active: true});
	  	new_data_obj.save()
	  		.catch(function(){
					console.log('err');
				})
				.then(function(saved_obj){
					console.log('saved bootstrapDB!: ');
					data = saved_obj.data;
					db_id = saved_obj.id;
				})
	  } else {
	  	console.log('data object already exists! id: ', data_obj.id);
	  	data = data_obj.data;
	  	db_id = data_obj.id;
	  }
	});
}

mongoose.model('Data', dataSchema);

var Data = mongoose.model('Data');
Data.bootstrapDB();


app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});
app.use(serveStatic(__dirname + '/public'));

io.on('connection', function(socket){
	socket.on('command', function(cmd){
		console.log('cmd received', cmd);
		switch(cmd.action){
			case 'CREATE SECTION':
				var id = createSection(cmd.payload.name, data, cmd.payload.id);
				cmd.payload.id = id;
				Data.persistData(db_id, data);
  			io.emit('command', cmd);
				break;
			case 'UPDATE SECTION':
				updateSection(cmd.payload.id, cmd.payload.new_section, data);
				Data.persistData(db_id, data);
				io.emit('command', cmd);
				break;
			case 'DELETE SECTION':
				deleteSection(cmd.payload.id, data);
				Data.persistData(db_id, data);
				io.emit('command', cmd);
				break;
			case 'READ':
				console.log('user id: ', socket.id);
				Data.findById(db_id, function(err, data_obj) {
  				if (err) throw err;
  				cmd.payload.data = data_obj.data;
  				io.emit('command', cmd);
  			});
  			break;
  		case 'RESTART DB':
  			Data.findById(db_id, function(err, data_obj) {
  				if (err) throw err;
  				data_obj.remove(function(err){
  					console.log('removed')
  					Data.bootstrapDB();
  					cmd.payload.data = [
						{id: 's1', name: 'user', disabled: true, data: [
							{id: 'u1', name: 'user1', space_after: 'space', connections: [], data: [
								{id: "btn0", type: 'user', state: 0, table: [
									{class:'', row: [{render: 'Lauren'}]},
									{class:'', row: [{render: 'lmiller@lawfirm.com'}]}
								]},
							]},
						]},
						{id: 's2', name: 'task', disabled: true, data: [
							{id: 't1', name: 'task1', space_after: 'space', connections: ['p1'], data: [
								{id: 'btn3', type: 'task', state: 0, table: [
									{class:'', row:[{render: 'March 3, 2017', type:'date', date: [2017, 2, 3]}]},
									{class:'', row:[{render: 'Receive case'}]}
								]}
							]}
						]},
						{id: 's3', name: 'policy', disabled: true, data: [
							{id: 'p1', name: 'policy1', space_after: 'space', connections: ['t1'], data: [
								{id: 'btn6', type: 'policy', state: 0, table: [
									{class: '', name_key: 'task1', row: [{render: '0 days', type: 'time', add_time: [0, 0, 0], to_task: 'task1', state: 0}, {render: "Receive Case"}]},
									{class: '', name_key: 'task2', row: [{render: '6 days', type: 'time', add_time: [0, 0, 6], to_task: 'task1', state: 1}, {render: "Initial Status Letter"}]},
									{class: '', name_key: 'task3', row: [{render: '12 days', type: 'time', add_time: [1, 0, 12], to_task: 'task1', state: 2}, {render: "Preservation Letter"}]},
								]}
							]},
						]}
					];
  					io.emit('command', cmd);
  				});
  			});
  			break;
  		case 'CREATE_CARD':
				var id = createCard(cmd.payload.section_index, cmd.payload.card_name, data, cmd.payload.id);
				cmd.payload.id = id;
				Data.persistData(db_id, data);
  			io.emit('command', cmd);
				break;
			case 'UPDATE_CARD':
				updateCard(cmd.payload.index, cmd.payload.new_card, data);
				Data.persistData(db_id, data);
				io.emit('command', cmd);
				break;
			case 'DELETE_CARD':
				deleteCard(cmd.payload.section_index, cmd.payload.id, data);
				Data.persistData(db_id, data);
				io.emit('command', cmd);
				break;
			case 'user':
				cycleState(cmd.payload.id, cmd.payload.state_length, data);
				Data.persistData(db_id, data);
				io.emit('command', cmd);
				break;
			case 'task':
				cycleState(cmd.payload.id, cmd.payload.state_length, data);
				updatePolicies(cmd.payload.connections, cmd.payload.name, data);
				Data.persistData(db_id, data);
				io.emit('command', cmd);
				break;
			case 'policy':
				var id_arr = updateTasks(cmd.payload.connections, cmd.payload.id, data);
				setButtonState(cmd.payload.current_date, cmd.payload.policy_id, data);
				cmd.payload.id_arr = id_arr;
				Data.persistData(db_id, data);
				io.emit('command', cmd);
				break;
			case 'login':
				if(cmd.payload.email==='test@mail'){
					Data.findById(db_id, function(err, data_obj) {
	  				if (err) throw err;
	  				cmd.payload.data = data_obj.data;
	  				io.to(socket.id).emit('command', cmd);
	  			});	
				} else {
					cmd.payload.data = []
	  			io.to(socket.id).emit('command', cmd);
				}
  			break;
			default: 
			console.log('action taken: ', cmd.action);
		}
  });
});

http.listen(process.env.PORT || 3000, function(){
	console.log('listening on port 3000');
});




//GENERAL STUFF
function findSectionIndex(data, id){
	var index = data.findIndex((section_obj)=>{
		return section_obj['id'] === id;
	});
	if(index === -1){
		data.map((section, section_index)=>{
			var card_index = section.data.findIndex((card)=>{
				return card['id'] === id;
			});
			if(card_index != -1){
				index = {section_index: section_index, card_index: card_index};	
			}
		});
	}
	if(index === -1){
		data.map((section, section_index)=>{
			section.data.map((card, card_index)=>{
				var btn_index = card.data.findIndex((btn)=>{
					return (btn.id===id)
				});
				if(btn_index != -1){
					index = {section_index: section_index, card_index: card_index, btn_index: btn_index};	
				}
			})
		});
	}
	return index;
}
function findKeyIndex(data, key){
	var table_index = data.data[0].table.findIndex((row)=>{
		return (row.name_key==key)
	});
	var index = {button_index: 0, table_index: table_index};
	return index;
}
function generateIndex(section_name='section'){
	var id = section_name.substr(0, 1)+"_"+Math.random().toString(36).substr(2, 9);
	return id;
}

//SECTION CRUD STUFF
function createSection(name, data, id=null){
	if(id===null){
		id = generateIndex();
	}
	data.push({id: id, name: name});
	return id;
}
exports.createSection = createSection;
function updateSection(id, new_section, data){
	var index = findSectionIndex(data, id);
	data[index] = JSON.parse(new_section);
}
exports.updateSection = updateSection;
function deleteSection(id, data){
	var index = findSectionIndex(data, id);
	data.splice(index, 1);
}
exports.deleteSection = deleteSection;

//CARD STUFF
function createCard(section_index, name, data, id=null, connections_arr=null, table_arr=null){
	if(id===null){
		id = generateIndex(data[section_index].name);
	}
	var btn_id = 'btn_' + id;
	var connections = [];
	if(connections_arr){
		connections = connections_arr;
	}
	var table = [
		{class:'', row:[{render: "Default text"}]},
		{class:'', row:[{render: "Default text"}]}
	];
	if(table_arr){
		table = table_arr;
	}
	data[section_index].data.push({id: id, name: name, space_after: 'space', connections: connections, data: [
		{id: btn_id, type: data[section_index].name, state: 0, table: table}
	]});
	return id;
}
function updateCard(index, new_card, data){
	console.log('i made it in update card');
	data[index.section_index].data[index.card_index] = JSON.parse(new_card);
}
function deleteCard(section_index, id, data){
	var found_index = findSectionIndex(data[section_index].data, id);
	data[section_index].data.splice(found_index, 1);
}

//STATUS STUFF
var button_states = {
	user: [
		{label_text: 'online', label_style: 'normal', icon: '', icon_background: '', icon_color: ''},
		{label_text: 'online', label_style: 'normal', icon: '', icon_background: 'lime', icon_color: ''},
	],
	task: [
		{label_text: 'todo', label_style: 'normal', icon: '', icon_background: '', icon_color: ''},
		{label_text: 'todo', label_style: 'checked', icon: 10003, icon_background: '', icon_color: ''},
	],
	policy: [
		{label_text: 'status1', label_style: 'normal', icon: '9679', icon_background: '', icon_color: 'green'},
		{label_text: 'status2', label_style: 'normal', icon: '9679', icon_background: '', icon_color: 'orange'},
		{label_text: 'status3', label_style: 'normal', icon: '9679', icon_background: '', icon_color: 'FireBrick'},
	]
};
function cycleState(id, state_length, data){
	console.log("data: ", data);
	console.log("id: ", id);
	var index = findSectionIndex(data, id);
	console.log("index: ", index);
	data[index.section_index].data[index.card_index].data[index.btn_index].state++
	if(data[index.section_index].data[index.card_index].data[index.btn_index].state>=state_length){
		data[index.section_index].data[index.card_index].data[index.btn_index].state=0
	}
}
function setButtonState(current_date, policy_id, data){
	console.log("current_date: ", current_date);
	console.log("policy_id: ", policy_id);
	console.log("data: ", data);
	var index = findSectionIndex(data, policy_id);
	var policy = data[index.section_index].data[index.card_index].data[0];
	policy.table.map((subtask, subtask_index)=>{
		subtask.row.map((data_cell, data_cell_index)=>{
			if(data_cell.type==='date'){
				var compare_dates = compareDates(current_date, data_cell.date);
				if(compare_dates===1){
					data[index.section_index].data[index.card_index].data[0].state = data_cell.state;
				}
			}
		});
	});
}
function compareDates(date1, date2){
	for (var i = 0; i < date1.length; i++) {
		if(date1[i]>date2[i]) return 1;
		if(date1[i]<date2[i]) return -1;
	};
	return 0;
}

//BUTTON STUFF
function updatePolicies(connections, key, data){
	connections.map((connection_id)=>{
		var index = findSectionIndex(data, connection_id);
		var key_index = findKeyIndex(data[index.section_index].data[index.card_index], key);
		
		if(data[index.section_index].data[index.card_index].data[key_index.button_index].table[key_index.table_index].class == ''){
			data[index.section_index].data[index.card_index].data[key_index.button_index].table[key_index.table_index].class = 'strike';
		} else {
			data[index.section_index].data[index.card_index].data[key_index.button_index].table[key_index.table_index].class = '';
		}
	});
}
function updateTasks(connections, id, data, id_arr=[]){
	var new_ids_arr = [];
	var index = findSectionIndex(data, id);
	var connections_dates = [];
	var names = connections.map((connection_id)=>{
		var connection_index = findSectionIndex(data, connection_id);
		if(data[connection_index.section_index].data[connection_index.card_index].data[0].type=='task'){
			var date = findDate(data[connection_index.section_index].data[connection_index.card_index].data[0].table);
			connections_dates.push({name: data[connection_index.section_index].data[connection_index.card_index].name, date: date});
		}
		return data[connection_index.section_index].data[connection_index.card_index].name;
	});
	if(connections_dates.length===0) return
	var task_section_index = data.findIndex((section)=>{
		return (section.name=='task');
	});
	var new_connections = [data[index.section_index].data[index.card_index].id];
	data[index.section_index].data[index.card_index].data[0].table.map((row, row_index)=>{
		var task_exist_index = names.findIndex((name)=>{
			return (name==row.name_key)
		});
		if(task_exist_index!=-1 && data[index.section_index].data[index.card_index].data[0].table[row_index].row[0].type==='time'){
			new_ids_arr.push(data[index.section_index].data[index.card_index].id);
			data[index.section_index].data[index.card_index].data[0].table[row_index].row[0].render =renderDate(connections_dates[task_exist_index].date);
			data[index.section_index].data[index.card_index].data[0].table[row_index].row[0].date =connections_dates[task_exist_index].date;
			data[index.section_index].data[index.card_index].data[0].table[row_index].row[0].type = 'date';
		} else {
			var table = row.row.map((data_cell, data_cell_index)=>{
				var new_data_cell = interpretDataCell(data_cell, connections_dates);
				if(data_cell.type==='time'){
					data[index.section_index].data[index.card_index].data[0].table[row_index].row[data_cell_index].render = new_data_cell.render;
					data[index.section_index].data[index.card_index].data[0].table[row_index].row[data_cell_index].date = new_data_cell.date;
					data[index.section_index].data[index.card_index].data[0].table[row_index].row[data_cell_index].type = 'date';
				}
				return {class: '', row: [new_data_cell]}
			});
			var alt_id = null;
			if(id_arr[row_index]){
				alt_id = id_arr[row_index];
			};
			var new_id = createCard(task_section_index, row.name_key, data, alt_id, new_connections, table);
			new_ids_arr.push(new_id);

			var new_data = JSON.parse(JSON.stringify(data));
			new_data[index.section_index].data[index.card_index].connections.push(new_id);
			var new_card = JSON.stringify(new_data[index.section_index].data[index.card_index]);
			console.log('new card: ', new_card);
			updateCard(index, new_card, data);
		}
	});
	return new_ids_arr;
}

//DATE STUFF
function findDate(table){
			var date = [];
			table.map((task)=>{
				var index = task.row.findIndex((data_cell)=>{
					return (data_cell.type=='date')
				});
				if(index !=-1){
					date = task.row[index].date
				}
			});
			return date;
		}
		function interpretDataCell(data_cell, connections_dates){
			switch (data_cell.type){
				case 'time':
					var new_data_cell = data_cell;
					var index = connections_dates.findIndex((connection_date)=>{
						return (data_cell.to_task===connection_date.name)
					});
					var new_date, new_render;
					if(index!=-1){
						new_date = addDates(connections_dates[index].date, data_cell.add_time);

						new_render = renderDate(new_date);
					}
					return {render: new_render, date: new_date}
					break;
				default: 
					return data_cell;
			}
		}
		function addDates(critical_date, expression){
	    var critical_year = critical_date[0] + expression[0];
	    var critical_month = critical_date[1] + expression[1];
	    if (critical_month>12) {
	      critical_month = critical_month - 12;
	      critical_year = critical_year + 1;
	    }
	    var critical_day = critical_date[2];
	    var date = new Date(critical_year, critical_month, critical_day);
	    var days = expression[2];
	    var newdate = new Date(date.setTime(date.getTime()+days*86400000));
	    var deadline_year = newdate.getFullYear();
	    var deadline_month = newdate.getMonth();
	    var deadline_day = newdate.getDate();
	    var deadline = [deadline_year, deadline_month, deadline_day];
	    return deadline;
	  }
		function renderDate(date){
			var month_str = 'no month ';
			switch (date[1]){
				case 0: 
					month_str = 'January ';
					break;
				case 1:
					month_str = 'February ';
					break;
				case 2:
					month_str = 'March ';
					break;
				case 3:
					month_str = 'April ';
					break;
				case 4:
					month_str = 'May ';
					break;
				case 5:
					month_str = 'June ';
					break;
				case 6:
					month_str = 'July ';
					break;
				case 7:
					month_str = 'August ';
					break;
				case 8:
					month_str = 'September ';
					break;
				case 9:
					month_str = 'October ';
					break;
				case 10:
					month_str = 'November ';
					break;
				case 11:
					month_str = 'December ';
					break;
			}
			return month_str+date[2]+", "+date[0];
		}